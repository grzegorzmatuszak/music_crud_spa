import React, {useState} from "react";
import {NavLink} from "react-router-dom";
import {RoutingList} from "../../Shared/Router/RoutingList";
import {Router} from "../../Shared/Router/Router";
import {useAlert} from 'react-alert'

interface ISong {
    title: string;
    genre: string;
    ismn: string;
    year: string;
    id: string;
}

interface IComponentProps {
    song?: ISong;
}

export function SongListElem(props: IComponentProps) {
    const alert = useAlert();
    const [song, setSong] = useState(props.song);

    const deleteSong = () => {
        if (song) {
            Router.delete(RoutingList.DELETE_SONG, {}, {id: song.id})
                .then((response) => {
                    if (201 === response.status) {
                        alert.success("Data removed successfully")
                        setSong(undefined);
                        return;
                    }
                    alert.error("Data removed successfully")
            });
        }
    };

    return song ? (
        <tr>
            <td>{song.title}</td>
            <td>{song.genre}</td>
            <td>{song.ismn}</td>
            <td>{song.year}</td>
            <td>
            <NavLink to={`/song/edit/${song.id}`} className="btn btn-primary">Edit</NavLink>
            <button type="button" onClick={deleteSong} className="btn btn-danger">Delete</button>
            </td>
        </tr>
    ): null;
}
