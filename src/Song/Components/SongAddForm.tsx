import React, {useState} from "react";
import {Input} from "../../Shared/Components/Form/Input";
import {Select} from "../../Shared/Components/Form/Select";
import {NavLink} from "react-router-dom";
import {Form} from "../../Shared/Components/Form/Form";
import {FormActionEnum} from "../../Shared/Enum/FormActionEnum";
import {MultiSelect} from "../../Shared/Components/Form/MultiSelect";
import {PublisherOptions} from "../../Publisher/Form/PublisherOptions";
import {ArtistOptions} from "../../Artist/Form/ArtistOptions";

interface IComponentProps {
    redirect?: string;
    action: FormActionEnum;
    routing: string;
}

export function SongAddForm(props: IComponentProps) {
    const [formErrors, setFormErrors] = useState({
        title: [],
        genre: [],
        ismn: [],
        year: [],
        publisher_id: [],
        artists_ids : []
    });

    const [errors] = useState({
        "formErrors": setFormErrors,
    });

    const [formData] = useState( {
        title: '',
        genre: '',
        ismn: '',
        year: '',
        publisher_id: '',
        publisher_name: 'Select...',
        artists_ids : [],
        artists_name : 'Select...'
    });

    return (
        <div className="container">
            <h2>Add song</h2>
            <Form action={props.action} redirectPath={props.redirect} routing={props.routing} errors={errors}>
                <Input name="title" label="First name" value={formData.title}
                       errors={formErrors.title}/>
                <Input name="genre" label="Last name" value={formData.genre}
                       errors={formErrors.genre}/>
                <Input name="ismn" label="Ismn" value={formData.ismn} errors={formErrors.ismn}/>
                <Input name="year" label="Year" value={formData.year} errors={formErrors.year}/>
                <Select name="publisher_id" label="Publisher"
                        loadOptions={PublisherOptions}
                        errors={formErrors.publisher_id}/>
                <MultiSelect name="artists_ids" label="Artists"
                        loadOptions={ArtistOptions}
                             errors={formErrors.artists_ids}/>
                <button className="btn btn-primary" type="submit">Submit</button>
                <NavLink to="/songs" className="btn btn-info">Cancel</NavLink>
            </Form>
        </div>
    );
}
