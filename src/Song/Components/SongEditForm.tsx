import React, {useState} from "react";
import {Input} from "../../Shared/Components/Form/Input";
import {Select} from "../../Shared/Components/Form/Select";
import {NavLink} from "react-router-dom";
import {Form} from "../../Shared/Components/Form/Form";
import {FormActionEnum} from "../../Shared/Enum/FormActionEnum";
import {ISelectOptions} from "../../Shared/Components/Form/SelectOptionsInterface";
import {MultiSelect} from "../../Shared/Components/Form/MultiSelect";
import {PublisherOptions} from "../../Publisher/Form/PublisherOptions";
import {ArtistOptions} from "../../Artist/Form/ArtistOptions";

export interface ISongForm {
    title: string;
    genre: string;
    ismn: string;
    year: string;
    publisher_id: ISelectOptions;
    artists_ids: ISelectOptions[];
}

export type SongEditFormProps = {
    song: ISongForm;
    headerPrefix: string;
    redirect?: string;
    action: FormActionEnum;
    routing: string;
}

export function SongEditForm(props: SongEditFormProps) {
    const updateFormData = (field: string, value: any) => {
        setFormData({
            ...formData,
            ...{
                field: value
            }
        });
    };

    const handlePublisherChange = (selectedOption: ISelectOptions) => {
        updateFormData('publisher_id', selectedOption.value);
    };

    const handleArtistsChange = (selectedOption?: ISelectOptions) => {
        updateFormData("artists_ids", selectedOption? selectedOption.value : []);
    };
    const [formErrors, setFormErrors] = useState({
        title: [],
        genre: [],
        ismn: [],
        year: [],
        publisher_id: [],
        artists_ids : []
    });

    const [errors] = useState({
        "formErrors": setFormErrors,
    });

    const [formData, setFormData] = useState(props.song);

    return (
        <div className="container">
            <h2>{props.headerPrefix} song</h2>
            <Form action={props.action} redirectPath={props.redirect} routing={props.routing} errors={errors}>
                <Input name="title" label="First name" value={formData.title}
                       errors={formErrors.title}/>
                <Input name="genre" label="Last name" value={formData.genre}
                       errors={formErrors.genre}/>
                <Input name="ismn" label="Ismn" value={formData.ismn} errors={formErrors.ismn}/>
                <Input name="year" label="Year" value={formData.year} errors={formErrors.year}/>
                <Select onChange={handlePublisherChange} name="publisher_id" label="Publisher"
                        defaultValue={formData.publisher_id} loadOptions={PublisherOptions}
                        errors={formErrors.publisher_id}/>
                <MultiSelect onChange={handleArtistsChange} name="artists_ids" label="Artists"
                             defaultValue={formData.artists_ids} loadOptions={ArtistOptions}
                             errors={formErrors.artists_ids}/>
                <button className="btn btn-primary" type="submit">Submit</button>
                <NavLink to="/songs" className="btn btn-info">Cancel</NavLink>
            </Form>
        </div>
    );
}
