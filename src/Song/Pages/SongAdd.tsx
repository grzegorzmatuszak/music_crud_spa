import React from 'react';
import {SongAddForm} from "../Components/SongAddForm";
import {RoutingList} from "../../Shared/Router/RoutingList";
import {FormActionEnum} from "../../Shared/Enum/FormActionEnum";

export function SongAdd() {
    return (
        <div>
            <SongAddForm routing={RoutingList.POST_SONG} redirect={'/song/edit/'} action={FormActionEnum.add} />
        </div>
    );
}
