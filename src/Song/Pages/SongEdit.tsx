import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {ISongForm, SongEditForm} from "../Components/SongEditForm";
import {RoutingList} from "../../Shared/Router/RoutingList";
import {generateRoute, Router} from "../../Shared/Router/Router";
import {FormActionEnum} from "../../Shared/Enum/FormActionEnum";
import {ISelectOptions} from "../../Shared/Components/Form/SelectOptionsInterface";

interface RouteParams {
    id: string
}

interface ISongEmbedded {
    song_artists: { id: string, nick: string }[],
    publisher: { name: string, id: string }
}

interface ISong extends ISongForm {
    _embedded: ISongEmbedded
}

function getArtistsIds(res: ISong) {
    return Object.values(res._embedded.song_artists).map(({id, nick}) => ({
        value: id,
        label: nick
    } as ISelectOptions));
}

export function SongEdit() {
    const params = useParams<RouteParams>();
    const [song, setSong] = useState<ISongForm>();

    useEffect(() => {
        const getSong = function () {
            Router.fetch(RoutingList.GET_SONG, {}, {id: params.id})
                .then((res: ISong) => {
                    setSong({
                        title: res.title,
                        genre: res.genre,
                        ismn: res.ismn,
                        year: res.year,
                        publisher_id: {'label': res._embedded.publisher.name, 'value': res._embedded.publisher.id},
                        artists_ids: getArtistsIds(res),
                    });
                });
        };
        getSong();
    }, []);

    if (song) {
        const songEditFormProps = {
            routing: generateRoute(RoutingList.PUT_SONG, {id: params.id}),
            action: FormActionEnum.edit,
            song: song,
            headerPrefix: "Edit"
        };

        return <SongEditForm {...songEditFormProps} />;
    }

    return <div></div>;
}
