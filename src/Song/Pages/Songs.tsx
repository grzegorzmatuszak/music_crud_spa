import React, {useEffect, useState} from "react";
import {SongListElem} from '../Components/SongListElem';
import {Router} from "../../Shared/Router/Router";
import {RoutingList} from "../../Shared/Router/RoutingList";
import {NavLink} from "react-router-dom";

interface ISong {
    title: string;
    genre: string;
    ismn: string;
    year: string;
    id: string;
}

export function Songs() {
    const [songs, setSongs] = useState([]);
    useEffect(() => {
        Router.fetch(RoutingList.GET_SONGS)
            .then((res: any) => {
                setSongs(
                    res
                );
            });
    }, []);

    return (
        <div className="container">
            <h2>Songs list <NavLink to={'/song/new'} className="btn btn-success">Add</NavLink></h2>
            <table className="table table-striped table-hover">
                <thead>
                <tr>
                    <th>title</th>
                    <th>Genre</th>
                    <th>Ismn</th>
                    <th>Year</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                {songs.map((song: ISong) => (
                    <SongListElem key={song.id}
                        song={song}
                    />
                ))}
                </tbody>
            </table>
        </div>
    );
}
