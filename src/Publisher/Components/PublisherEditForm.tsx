import React, {useState} from "react";
import {Input} from "../../Shared/Components/Form/Input";
import {NavLink} from "react-router-dom";
import {Form} from "../../Shared/Components/Form/Form";
import {FormActionEnum} from "../../Shared/Enum/FormActionEnum";
import {IFieldErrorProps} from "../../Shared/Components/Form/FieldError";

export interface IPublisher {
    name: string;
    nip: string;
    address: string;
}

export type PublisherEditFormProps = {
    publisher?: IPublisher;
    headerPrefix: string;
    redirect?: string;
    action: FormActionEnum;
    routing: string;
}

export function PublisherEditForm(props: PublisherEditFormProps) {
    const [formErrors, setFormErrors] = useState({
        name: new Array<IFieldErrorProps>(),
        nip: new Array<IFieldErrorProps>(),
        address: new Array<IFieldErrorProps>()
    });

    const [errors] = useState({
        "formErrors": setFormErrors,
    });

    const [formData] = useState(props.publisher ? props.publisher : {
        name: '',
        nip: '',
        address: '',
    });

    return (
        <div className="container">
            <h2>{props.headerPrefix} publisher</h2>
            <Form action={props.action} redirectPath={props.redirect} routing={props.routing} errors={errors}>
                <Input name="name" label="Name" value={formData.name}
                       errors={formErrors.name}/>
                <Input name="nip" label="Nip" value={formData.nip}
                       errors={formErrors.nip}/>
                <Input name="address" label="Address" value={formData.address} errors={formErrors.address}/>
                <button className="btn btn-primary" type="submit">Submit</button>
                <NavLink to="/publishers" className="btn btn-info">Cancel</NavLink>
            </Form>
        </div>
    );
}
