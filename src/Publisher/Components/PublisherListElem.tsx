import React, {useState} from "react";
import {NavLink} from "react-router-dom";
import {RoutingList} from "../../Shared/Router/RoutingList";
import {Router} from "../../Shared/Router/Router";
import {useAlert} from 'react-alert'

interface IPublisher {
    id: string;
    name: string;
    nip: string;
    address: string;
}

interface IComponentProps {
    publisher?: IPublisher;
}

export function PublisherListElem(props: IComponentProps) {
    const alert = useAlert();
    const [publisher, setPublisher] = useState(props.publisher);

    const deletePublisher = () => {
        if (publisher) {
            Router.delete(RoutingList.DELETE_PUBLISHER, {}, {id: publisher.id})
                .then((response) => {
                    if (201 === response.status) {
                        alert.success("Data removed successfully")
                        setPublisher(undefined);
                        return;
                    }
                    alert.error("Data removed successfully")
            });
        }
    };

    return publisher ? (
        <tr>
            <td>{publisher.name}</td>
            <td>{publisher.nip}</td>
            <td>{publisher.address}</td>
            <td>
            <NavLink to={`/publisher/edit/${publisher.id}`} className="btn btn-primary">Edit</NavLink>
            <button type="button" onClick={deletePublisher} className="btn btn-danger">Delete</button>
            </td>
        </tr>
    ): null;
}
