import React, {useEffect, useState} from "react";
import {PublisherListElem} from '../Components/PublisherListElem';
import {Router} from "../../Shared/Router/Router";
import {RoutingList} from "../../Shared/Router/RoutingList";
import {NavLink} from "react-router-dom";

interface IPublisher {
    name: string;
    nip: string;
    address: string;
    id: string;
}

export function Publishers() {
    const [publishers, setPublishers] = useState([]);
    useEffect(() => {
        Router.fetch(RoutingList.GET_PUBLISHERS)
            .then((res: any) => {
                setPublishers(
                    res
                );
            });
    }, []);

    return (
        <div className="container">
            <h2>Publishers list <NavLink to={'/publisher/new'} className="btn btn-success">Add</NavLink></h2>
            <table className="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Nip</th>
                    <th>Address</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                {publishers.map((publisher: IPublisher) => (
                    <PublisherListElem key={publisher.id}
                        publisher={publisher}
                    />
                ))}
                </tbody>
            </table>
        </div>
    );
}
