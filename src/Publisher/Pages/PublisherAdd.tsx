import React from 'react';
import {PublisherEditForm} from "../Components/PublisherEditForm";
import {RoutingList} from "../../Shared/Router/RoutingList";
import {FormActionEnum} from "../../Shared/Enum/FormActionEnum";

const publisherEditFormProps = {
    routing: RoutingList.POST_PUBLISHER,
    redirect: '/publisher/edit/',
    action: FormActionEnum.add,
    headerPrefix: "Add"
};

export function PublisherAdd() {
    return <PublisherEditForm {...publisherEditFormProps}/>;
}
