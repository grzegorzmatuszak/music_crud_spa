import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {IPublisher, PublisherEditForm} from "../Components/PublisherEditForm";
import {RoutingList} from "../../Shared/Router/RoutingList";
import {generateRoute, Router} from "../../Shared/Router/Router";
import {FormActionEnum} from "../../Shared/Enum/FormActionEnum";

interface RouteParams {
    id: string
}

export function PublisherEdit() {
    const params = useParams<RouteParams>();
    const [publisher, setPublisher] = useState<IPublisher>();

    useEffect(() => {
        const getPublisher = function () {
            Router.fetch(RoutingList.GET_PUBLISHER, {}, {id: params.id})
                .then((res: any) => {
                    setPublisher(res)
                });
        };
        getPublisher();
    }, []);

    const publisherEditFormProps = {
        routing: generateRoute(RoutingList.PUT_PUBLISHER, {id: params.id}),
        action: FormActionEnum.edit,
        publisher: publisher,
        headerPrefix: "Edit"
    };

    return publisher ? <PublisherEditForm {...publisherEditFormProps}/> : <div></div>;
}
