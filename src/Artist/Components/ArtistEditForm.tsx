import React, {useState} from "react";
import {Input} from "../../Shared/Components/Form/Input";
import {NavLink} from "react-router-dom";
import {Form} from "../../Shared/Components/Form/Form";
import {FormActionEnum} from "../../Shared/Enum/FormActionEnum";
import {IFieldErrorProps} from "../../Shared/Components/Form/FieldError";

export interface IArtist {
    first_name: string;
    last_name: string;
    nick: string;
}

export type ArtistEditFormProps = {
    artist?: IArtist;
    headerPrefix: string;
    redirect?: string;
    action: FormActionEnum;
    routing: string;
}

export function ArtistEditForm(props: ArtistEditFormProps) {
    const [formErrors, setFormErrors] = useState({
        first_name: new Array<IFieldErrorProps>(),
        last_name: new Array<IFieldErrorProps>(),
        nick: new Array<IFieldErrorProps>()
    });

    const [errors] = useState({
        "formErrors": setFormErrors,
    });


    const [formData] = useState(props.artist ? props.artist : {
        first_name: '',
        last_name: '',
        nick: '',
    });

    return (
        <div className="container">
            <h2>{props.headerPrefix} artist</h2>
            <Form action={props.action} redirectPath={props.redirect} routing={props.routing} errors={errors}>
                <Input name="first_name" label="First name" value={formData.first_name}
                       errors={formErrors.first_name}/>
                <Input name="last_name" label="Last name" value={formData.last_name}
                       errors={formErrors.last_name}/>
                <Input name="nick" label="Nick" value={formData.nick} errors={formErrors.nick}/>
                <button className="btn btn-primary" type="submit">Submit</button>
                <NavLink to="/artists" className="btn btn-info">Cancel</NavLink>
            </Form>
        </div>
    );
}
