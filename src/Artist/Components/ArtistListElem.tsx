import React, {useState} from "react";
import {NavLink} from "react-router-dom";
import {RoutingList} from "../../Shared/Router/RoutingList";
import {Router} from "../../Shared/Router/Router";
import {useAlert} from 'react-alert'

interface IArtist {
    first_name: string;
    last_name: string;
    id: string;
    nick: string;
}

interface IComponentProps {
    artist?: IArtist;
}

export function ArtistListElem(props: IComponentProps) {
    const alert = useAlert();
    const [artist, setArtist] = useState(props.artist);

    const deleteArtist = () => {
        if (artist) {
            Router.delete(RoutingList.DELETE_ARTIST, {}, {id: artist.id})
                .then((response) => {
                    if (201 === response.status) {
                        alert.success("Data removed successfully")
                        setArtist(undefined);
                        return;
                    }
                    alert.error("Data removed successfully")
            });
        }
    };

    return artist ? (
        <tr>
            <td>{artist.first_name}</td>
            <td>{artist.last_name}</td>
            <td>{artist.nick}</td>
            <td>
            <NavLink to={`/artist/edit/${artist.id}`} className="btn btn-primary">Edit</NavLink>
            <button type="button" onClick={deleteArtist} className="btn btn-danger">Delete</button>
            </td>
        </tr>
    ): null;
}
