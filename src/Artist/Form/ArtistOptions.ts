import {Router} from "../../Shared/Router/Router";
import {RoutingList} from "../../Shared/Router/RoutingList";
import {ISelectOptions} from "../../Shared/Components/Form/SelectOptionsInterface";

export function ArtistOptions() {
    return Router.fetch(RoutingList.GET_ARTISTS)
        .then((res: { id: string, nick: string }[]) => {
            return Object.values(res).map(({id, nick}) => ({value: id, label: nick} as ISelectOptions));
        });
}
