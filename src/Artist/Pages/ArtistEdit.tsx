import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {ArtistEditForm, IArtist} from "../Components/ArtistEditForm";
import {RoutingList} from "../../Shared/Router/RoutingList";
import {generateRoute, Router} from "../../Shared/Router/Router";
import {FormActionEnum} from "../../Shared/Enum/FormActionEnum";

interface RouteParams {
    id: string
}

export function ArtistEdit() {
    const params = useParams<RouteParams>();
    const [artist, setArtist] = useState<IArtist>();

    useEffect(() => {
        const getArtist = function () {
            Router.fetch(RoutingList.GET_ARTIST, {}, {id: params.id})
                .then((res: any) => {
                    setArtist(res)
                });
        };
        getArtist();
    }, []);

    const artistEditFormProps = {
        routing: generateRoute(RoutingList.PUT_ARTIST, {id: params.id}),
        redirect: '/artist/edit/',
        action: FormActionEnum.edit,
        headerPrefix: "Edit",
        artist: artist
    };

    return artist ? <ArtistEditForm {...artistEditFormProps}/> : <div></div>;
}
