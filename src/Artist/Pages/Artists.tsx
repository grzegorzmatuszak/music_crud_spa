import React, {useEffect, useState} from "react";
import {ArtistListElem} from '../Components/ArtistListElem';
import {Router} from "../../Shared/Router/Router";
import {RoutingList} from "../../Shared/Router/RoutingList";
import {NavLink} from "react-router-dom";

interface IArtist {
    first_name: string;
    last_name: string;
    id: string;
    nick: string;
}

export function Artists() {
    const [artists, setArtists] = useState([]);
    useEffect(() => {
        Router.fetch(RoutingList.GET_ARTISTS)
            .then((res: any) => {
                setArtists(
                    res
                );
            });
    }, []);

    return (
        <div className="container">
            <h2>Artists list <NavLink to={'/artist/new'} className="btn btn-success">Add</NavLink></h2>
            <table className="table table-striped table-hover">
                <thead>
                <tr>
                    <th>First name</th>
                    <th>last name</th>
                    <th>Nick</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                {artists.map((artist: IArtist) => (
                    <ArtistListElem key={artist.id}
                        artist={artist}
                    />
                ))}
                </tbody>
            </table>
        </div>
    );
}
