import React from 'react';
import {ArtistEditForm} from "../Components/ArtistEditForm";
import {RoutingList} from "../../Shared/Router/RoutingList";
import {FormActionEnum} from "../../Shared/Enum/FormActionEnum";

const artistEditFormProps = {
    routing: RoutingList.POST_ARTIST,
    redirect: '/artist/edit/',
    action: FormActionEnum.add,
    headerPrefix: "Add"
};

export function ArtistAdd() {
    return (
        <div>
            <ArtistEditForm {...artistEditFormProps} />
        </div>
    );
}
