import React from "react";

export function Home() {
    return (
        <div className="container">
            <h2>Music Crud</h2>
            <article>
                <p>Some text to show because empty site looks incorrect</p>
                <p>With this app you can manage publishers, songs and artists</p>
            </article>
        </div>
    );
}
