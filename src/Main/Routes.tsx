import {HashRouter, Route} from "react-router-dom";
import {Home} from "./Home";
import {Artists} from "../Artist/Pages/Artists";
import {ArtistAdd} from "../Artist/Pages/ArtistAdd";
import {ArtistEdit} from "../Artist/Pages/ArtistEdit";
import {Publishers} from "../Publisher/Pages/Publishers";
import React from "react";
import {PublisherAdd} from "../Publisher/Pages/PublisherAdd";
import {PublisherEdit} from "../Publisher/Pages/PublisherEdit";
import {Songs} from "../Song/Pages/Songs";
import {SongEdit} from "../Song/Pages/SongEdit";
import {SongAdd} from "../Song/Pages/SongAdd";

export const Routes = () => {
    return (
        <HashRouter>
            <div>
                <Route exact path="/" component={Home}/>

                <Route path="/artists" component={Artists}/>
                <Route path="/artist/new" component={ArtistAdd}/>
                <Route path="/artist/edit/:id" component={ArtistEdit}/>

                <Route path="/publishers" component={Publishers}/>
                <Route path="/publisher/new" component={PublisherAdd}/>
                <Route path="/publisher/edit/:id" component={PublisherEdit}/>

                <Route path="/songs" component={Songs}/>
                <Route path="/song/new" component={SongAdd}/>
                <Route path="/song/edit/:id" component={SongEdit}/>
            </div>
        </HashRouter>
    );
};
