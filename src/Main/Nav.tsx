import {HashRouter, NavLink} from "react-router-dom";
import React from "react";

export const Nav = () => {
    return (
        <HashRouter>
            <div className="container-fluid">
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item"><NavLink to="/" className="nav-link">Home</NavLink></li>
                        <li className="nav-item"><NavLink to="/songs" className="nav-link">Songs</NavLink></li>
                        <li className="nav-item"><NavLink to="/artists" className="nav-link">Artists</NavLink></li>
                        <li className="nav-item"><NavLink to="/publishers" className="nav-link">Publishers</NavLink></li>
                    </ul>
                </nav>
            </div>
        </HashRouter>
    );
};
