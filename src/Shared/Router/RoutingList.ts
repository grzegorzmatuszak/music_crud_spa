export const RoutingList = {
    GET_ARTISTS: '/artists',
    GET_ARTIST: '/artist/{id}',
    PUT_ARTIST: '/artist/{id}',
    POST_ARTIST: '/artist',
    DELETE_ARTIST: '/artist/{id}',

    GET_PUBLISHERS: '/publishers',
    GET_PUBLISHER: '/publisher/{id}',
    PUT_PUBLISHER: '/publisher/{id}',
    POST_PUBLISHER: '/publisher',
    DELETE_PUBLISHER: '/publisher/{id}',

    GET_SONGS: '/songs',
    GET_SONG: '/song/{id}',
    PUT_SONG: '/song/{id}',
    POST_SONG: '/song',
    DELETE_SONG: '/song/{id}',
};
